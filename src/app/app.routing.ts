import { ModuleWithProviders } from "@angular/core";
import { Route, RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { PanelDocumentoComponent } from "./catalogo-documentos/components/panel-documento/panel-documento.component";
import { AllCatalogosViewComponent } from "./catalogo-documentos/pages/all-catalogos-view/all-catalogos-view.component";
import { HomeItemsComponent } from "./home/components/home-items/home-items.component";

const appRoutes: Routes = [
  { path: 'catalogo', component: AllCatalogosViewComponent},
  { path: 'home', component: HomeItemsComponent},
  { path: 'configdocument/new', component: PanelDocumentoComponent},
  { path: 'configdocument/edit', component: PanelDocumentoComponent},
  { path: '**', redirectTo:'home' }

];

export const APP_ROUTING: ModuleWithProviders<Route> = RouterModule.forRoot(appRoutes);
