import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeItemsComponent } from './components/home-items/home-items.component';

const routes: Routes = [
  {
    path:'',
    children: [
      { path:'', component: HomeItemsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class HomeRoutingModule { }
