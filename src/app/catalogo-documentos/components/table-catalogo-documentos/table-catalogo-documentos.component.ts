import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CatalogoDocumentos } from '../../interfaces/catalogo-documentos.interface';
import { DeleteCatalogos } from '../../interfaces/delete-catalogos.interface';
import { IntegrationServerBackService } from '../../services/integration-server-back.service';

@Component({
  selector: 'app-table-catalogo-documentos',
  templateUrl: './table-catalogo-documentos.component.html',
  styleUrls: ['./table-catalogo-documentos.component.css'],
})
export class TableCatalogoDocumentosComponent implements OnInit {
  listadoCatalogos: CatalogoDocumentos[] = [];
  catalogoDocumentos!: CatalogoDocumentos;
  selectedCatalogos: CatalogoDocumentos[] = [];
  submitted!: boolean;
  catalogoDialog!: boolean;

  constructor(
    private integrationServerService: IntegrationServerBackService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private route: Router
  ) {
    this.integrationServerService
      .getListadoCatalogoDocumentos()
      .subscribe((result) => {
        this.setListadoCatalogos(result);
      });
  }

  ngOnInit(): void {}

  deleteSelectedCatalogos() {
    this.catalogoDocumentos = {};
    this.submitted = false;
    this.catalogoDialog = true;
  }

  hideDialogDelete() {
    this.catalogoDialog = false;
  }

  confirmDelete() {
    let idsAEliminar: string[] = [];
    this.selectedCatalogos.forEach((catalogoDocs) =>
      idsAEliminar.push(catalogoDocs.id!)
    );

    this.integrationServerService
      .deleteListadosCatalogoDocumentos(idsAEliminar)
      .subscribe((result) => {
        /*
        let eliminadosCorrectamente: string[] = [];

        for(let entry of result.entries()) {
          let estado: string = entry[1];
          if(estado.includes('Eliminado')){
            eliminadosCorrectamente.push(entry[0])
          }

        }
        */
        this.listadoCatalogos = this.listadoCatalogos.filter(
          (val) => !this.selectedCatalogos.includes(val)
        );
        this.selectedCatalogos = [];
        this.messageService.add({
          severity: 'success',
          summary: 'Successful',
          detail: 'Catalogos eliminados',
          life: 3000,
        });
      });

    this.catalogoDialog = false;
  }

  /*
  deleteSelectedCatalogos() {
    console.log('b')
    this.confirmationService.confirm({
      message: 'Se eliminaran los elementos seleccionados',
      header: '¡Atención!',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        console.log('Confirm delete')
        this.messageService.add({severity:'success', summary:'Successful', detail:'Catalogos eliminados', life:3000});
      }
    })
  }*/

  goToNewDocument() {
    this.route.navigate(['/configdocument']);
  }

  //Setters
  setListadoCatalogos(data: CatalogoDocumentos[]) {
    this.listadoCatalogos = data;
  }
}
