import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCatalogoDocumentosComponent } from './table-catalogo-documentos.component';

describe('TableCatalogoDocumentosComponent', () => {
  let component: TableCatalogoDocumentosComponent;
  let fixture: ComponentFixture<TableCatalogoDocumentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableCatalogoDocumentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCatalogoDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
