import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelDocumentoComponent } from './panel-documento.component';

describe('PanelDocumentoComponent', () => {
  let component: PanelDocumentoComponent;
  let fixture: ComponentFixture<PanelDocumentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelDocumentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
