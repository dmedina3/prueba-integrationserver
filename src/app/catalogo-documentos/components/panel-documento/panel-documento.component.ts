import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IntegrationServerBackService } from '../../services/integration-server-back.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-documento',
  templateUrl: './panel-documento.component.html',
  styleUrls: ['./panel-documento.component.css']
})
export class PanelDocumentoComponent implements OnInit {

  nuevo:boolean = false;
  token;
  get ItemsMetodosCarga():FormArray{
    return this.configDocumentForm.get("metodosGoCargas") as FormArray
  }
  get ItemsMetodosDescarga():FormArray{
    return this.configDocumentForm.get("metodosGoDescargas") as FormArray
  }
  configDocumentForm: FormGroup = this.fb.group({
    grupo: ['', [Validators.required] ],
    publicacion: ['', [Validators.required] ],
    apiUpload: ['', [Validators.required] ],
    apiDownload: ['', [Validators.required] ],
    jsonName: ['', [Validators.required] ],
    mail: ['', [Validators.required] ],
    metodosGoCargas: this.fb.array([]),
    metodosGoDescargas: this.fb.array([])
  });
  gruposSelect:any[] = [];
  metodosCargas:any[] =[]
  metodosDescargas:any[] =[]
  btnSave: boolean = false;

  constructor(private router: Router, private fb: FormBuilder, private messageService : MessageService, private panelDocumentoService: IntegrationServerBackService){
    this.setGrupoDataSelector()
    this.token = localStorage.getItem('token')
  }
  ngOnInit(): void {
    this.setGrupoDataSelector()
    this.nuevo = this.router.url.includes('new')
  }
  campoEsValido( campo: string){
    return  this.configDocumentForm.controls[campo].errors &&
            this.configDocumentForm.controls[campo].touched
  }
  onSubmit():void {
    const self = this;
    if(this.btnSave){

      this.configDocumentForm.markAllAsTouched();

      if(this.configDocumentForm.valid){
        if(this.nuevo){
          this.panelDocumentoService.saveDocumento(this.configDocumentForm.value).then(() =>{
            self.navigateToCatalog()
          })
        }else{
          const idDoc = this.router.parseUrl('id')
          this.panelDocumentoService.updateDocumento(Object.assign({}, {id: idDoc}, this.configDocumentForm.value ))
            .then(()=>{
              self.navigateToCatalog()      
            })
        }
      } else {
        this.showMessageWarning();
        this.btnSave = false;
      }

    } else {
      this.showMessageError();
    }
  }
  private navigateToCatalog(){
    this.showMessageSuccess();
    this.btnSave = false;
    this.router.navigateByUrl('/catalogo')
  }
  setGrupoDataSelector(){
    this.gruposSelect = [ {CODIGO: 'COMERCIAL', caption: 'Comercial'},
                          {CODIGO: 'AGRO', caption: 'Agro'}
                        ]
  }
  onSaveClick( val : boolean){
    this.btnSave = val;
  }
  showMessageSuccess(){
    this.messageService.add({key:'msj',severity:'success', summary:'Guardado correctamente'});
  }

  showMessageWarning(){
    this.messageService.add({key:'msj', severity:'warn', summary:'No se puede guardar', detail:'Campos requeridos vacios'});
  }

  showMessageError(){
    this.messageService.add({key:'msj', severity:'error', summary:'Error al guardar', detail:'Error genérico'});
  }

}
