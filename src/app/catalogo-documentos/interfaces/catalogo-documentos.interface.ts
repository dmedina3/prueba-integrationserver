export interface CatalogoDocumentos {
  id?: string,
  grupo?: string,
  documento?: string,
  metodosGoDescargas?: string,
  metodosGoCargas?: string,
  apiUpload?: string,
  apiDownload?: string,
  descJson?: string,
  mail?: string
}
