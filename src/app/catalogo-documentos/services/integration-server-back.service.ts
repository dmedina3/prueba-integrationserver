import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http'
import { lastValueFrom, map, Observable } from 'rxjs';
import { CatalogoDocumentos } from '../interfaces/catalogo-documentos.interface';
import { DeleteCatalogos } from '../interfaces/delete-catalogos.interface';

@Injectable({
  providedIn: 'root'
})
export class IntegrationServerBackService {
  

  private token = localStorage.getItem('token');
  private base_url = 'http://localhost:8080/api/1/integrationserver-back/'

  constructor(private http: HttpClient) { }

  public getListadoCatalogoDocumentos():Observable<CatalogoDocumentos[]>{
    const url: string = `${ this.base_url}catalogoDocumentos?access_token=${this.token}`;
    return this.http.get<CatalogoDocumentos[]>(url);
  }

  public deleteListadosCatalogoDocumentos(selectedCatalogos: string[]):Observable< Map<String,String> >{
    const body : DeleteCatalogos = {};
    body.ids = selectedCatalogos;

    const url: string = `${ this.base_url}catalogoDocumentos/delete?access_token=${this.token}`;
    return this.http.post< Map<String,String> >(url, body)
  }

  async saveDocumento(body: any): Promise<any> {
    const url = `${this.base_url}/catalogoDocumentos`
    return await lastValueFrom(this.http.post(url, body).pipe(map(res => res )))
  }
  async updateDocumento(body: any) {
    const url = `${this.base_url}/catalogoDocumentos`
    return await lastValueFrom(this.http.put(url, body).pipe(map(res => res )))
  }

}
