import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllCatalogosViewComponent } from './pages/all-catalogos-view/all-catalogos-view.component';

const routes: Routes = [
  {
    path:'',
    children : [
      { path: 'all', component: AllCatalogosViewComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CatalogoDocumentosRoutingModule { }
