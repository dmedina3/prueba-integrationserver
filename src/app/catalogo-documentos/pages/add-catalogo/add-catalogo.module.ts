import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCatalogoRoutingModule } from './add-catalogo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddCatalogoRoutingModule
  ]
})
export class AddCatalogoModule { }
