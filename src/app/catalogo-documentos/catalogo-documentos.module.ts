import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CatalogoDocumentosRoutingModule } from './catalogo-documentos-routing.module';
import { PrimengModule } from '../primeng/primeng.module';
import { TableCatalogoDocumentosComponent } from './components/table-catalogo-documentos/table-catalogo-documentos.component';
import { AllCatalogosViewComponent } from './pages/all-catalogos-view/all-catalogos-view.component';
import { ButtonComponent } from './components/button/button.component';
import { PanelDocumentoComponent } from './components/panel-documento/panel-documento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TableCatalogoDocumentosComponent,
    AllCatalogosViewComponent,
    ButtonComponent,
    PanelDocumentoComponent
  ],
  imports: [
    CommonModule,
    CatalogoDocumentosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PrimengModule
  ]
})
export class CatalogoDocumentosModule { }
