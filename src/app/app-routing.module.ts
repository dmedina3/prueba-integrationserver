import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'catalogo',
    loadChildren: () => import ('./catalogo-documentos/catalogo-documentos.module').then( m => m.CatalogoDocumentosModule)
  },
  {
    path: 'home',
    loadChildren: () => import ('./home/home.module').then( m => m.HomeModule)
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
